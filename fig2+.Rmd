---
title: "Fig2+"
author: "Øystein Monsen"
date: '2022-09-28'
output: html_document
---


```{r setup, include=FALSE}

library(data.table)
library(stringr)
library(seqinr)
library(GenomicRanges)

tefasta<-read.fasta("data/SIMON_ANNOT_INFO/TE_lib.fa")

lengths<-data.table(cbind(getName(tefasta), getLength(tefasta)))
lengths[,TEfam:=str_extract(V1, ".+(?=#)")][,V1:=NULL]
colnames(lengths)[1]<-"consensuslength"
setkey(lengths, TEfam)

satannots<-fread("data/SIMON_ANNOT_INFO/rm_annot/chrom/Simon_Final2021_CHR.fasta.ori.out")
chrtrf<-fread("data/SIMON_ANNOT_INFO/TRF-stuff/simonchr_align.gff")

telpeaks<-fread("data/telomereheads.bed")
colnames(telpeaks)[1]<-"chrom"

gr_sat<-GenomicRanges::reduce(makeGRangesFromDataFrame(satannots[,.("chrom"=V5, "start"=V6, "end"=V7)]))
gr_trf<-GenomicRanges::reduce(makeGRangesFromDataFrame(chrtrf[,.("chrom"=V1, "start"=V5, "end"=V6)]))
gr_tels<-makeGRangesFromDataFrame(telpeaks)

satdist<-distanceToNearest(gr_sat, gr_tels)
trfdist<-distanceToNearest(gr_trf, gr_tels)

mcols(gr_sat)$distance<-as.data.table(satdist)$distance
mcols(gr_trf)$distance<-as.data.table(trfdist)$distance


TEs<-fread("data/SIMON_ANNOT_INFO/rm_annot/chrom/tes_mask/Simon_TEs.gff", fill=TRUE)
TEs[,c("V1", "V3", "V4","V8", "V9", "V11", "V12", "V13", "V14", "V16"):=NULL]
colnames(TEs)<-c("div", "chrom", "start", "end", "TEfam", "ID")
TEs[,ID:=.I]
TEs[,tag:=str_extract(TEfam, ".+(?=_)")]
TEs[,width:=end-start][,fambp:=sum(width), by=TEfam][,class:="DNA"][tag=="XXX",class:="Unknown"][tag %like% "R", class:="Retrotransposon"]
setkey(TEs, TEfam)
TEs<-TEs[lengths]
bpfam<-unique(TEs[,.(TEfam, tag, fambp)])
bpfam[,classbp:=sum(fambp), by=tag]
tagfam<-unique(bpfam[,.(tag, classbp)])

nulltags<-TEs[,.N, by=tag][N>1000][,proportion:=N/sum(N)][,nlog:=log10(N)][,class:="DNA"][tag=="XXX",class:="Unknown"][tag %like% "R", class:="Retrotransposon"]
setkey(nulltags, tag)
setkey(tagfam, tag)

nulltags<-nulltags[tagfam]
nulltags<-nulltags[!is.na(N)]
nulltags[,bpprop:=classbp/sum(classbp)]

#FIG1: TE-fordeling, group by class, anten proportion eller nlog OG SÅ bpprop eller logbp; så store ridgeplots etter tag, EV insersjonsstorleiksfordeling etter tag. SatDNA/TR: arraystorleiksfordeling, mini- micro- satdna-fordeling, ridgeplot etter annotering. Berre å lage desse og pjåte dei inn, ta atterhald i morgon. KANSKJE EIT KROMOSOMDØME MED FORDELING? BARPLOT? HØYR MED DEI!




```

## R Markdown

mak plot
```{r plot}

library(stringr)
library(tidyverse)
library(utils)
library(plotly)
library(RColorBrewer)
library(cowplot)
library(ggridges)


theme_set(theme_gray()+theme(axis.line = element_line(size=0.5),
                             panel.background = element_rect(fill = NA, size = rel(20)),
                             panel.grid.minor = element_line(colour = NA),
                             axis.text = element_text(size=16),
                             axis.title = element_text(size = 18)))


nulltags
scaleFUN <- function(x) sprintf("%.0f", x)

plotable<-melt(nulltags, id.vars = c("tag", "class"), measure.vars = c("proportion", "bpprop"))
plotable[variable=="bpprop",variable:="BPs"][variable=="proportion",variable:="Insertions"]

plotable[,.(tag, value, variable)] %>%
  ggplot(aes(y=value, x=tag, fill = variable))  + 
  geom_bar(position = "dodge", stat = "identity") +
  xlab("Tag") +
  ylab("Proportion") +
  scale_fill_brewer(palette = "Dark2") +
  theme(strip.text.y = element_text(face = "bold"),
        axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1),
        legend.title = element_blank(),
        legend.text = element_text(size=14),
        plot.title = element_text(hjust = 0.5, size =22)) -> PROPORTIONS_BARPLOT
#ggsave(PROPORTIONS_BARPLOT, file ="/mnt/users/oymo/kristina_samarbeid/figurar/propbar.png", 
#       width = 40, 
#       height = 25,
#       units = "cm")


ridgeable<-TEs[tag %in% plotable$tag][,.(class, tag, TEfam, div)]
ridgeable$class <- factor(ridgeable$class, levels = c("Unknown", "Retrotransposon", "DNA"))

ridgeable %>% 
  ggplot(aes(x = div, y = tag, fill=class)) +
  geom_density_ridges(scale = 1.5) +
  xlab("Divergence from Consensus") +
  ylab("Tag") +
  scale_fill_brewer(palette = "Dark2") -> RIDGEPLOT  #+
  # theme(strip.text.y = element_text(face = "bold"),
  #       axis.text.y = element_text(size=16),
  #       axis.text.x = element_text(size=16),
  #       legend.title = element_blank(),
  #       legend.text = element_text(size=14),
  #       plot.title = element_text(hjust = 0.5, size =22)) -> RIDGEPLOT


#ggsave(RIDGEPLOT, file ="/mnt/users/oymo/kristina_samarbeid/figurar/ridgeplot.png", 
#       width = 40, 
#       height = 25,
#       units = "cm")

#insersjonsstorleik, boxplot
  
boxable<-TEs[tag %in% plotable$tag][,.(width, tag, class, consensuslength)]
boxable$consensuslength<-as.numeric(boxable$consensuslength)
boxable[,insertionsize:=(width/consensuslength)]
boxable %>%
  ggplot(aes(insertionsize,tag, fill = class)) + 
  geom_boxplot() + 
  ylab("Tag") +
  xlab("Proportion of consensus pr insertion") +
  theme(axis.text.x=element_text(angle=90, vjust = 0.5, hjust=1)) +
  scale_fill_brewer(palette = "Dark2") +
  xlim(0, 2) -> BOXPLOT# +
  # theme(strip.text.y = element_text(face = "bold"),
  #       axis.text.y = element_text(size=16),
  #       axis.text.x = element_text(size=16),
  #       legend.title = element_blank(),
  #       legend.text = element_text(size=14),
  #       plot.title = element_text(hjust = 0.5, size =22)) -> BOXPLOT

ggsave(BOXPLOT, file ="/mnt/users/oymo/kristina_samarbeid/figurar/boxplot.png", 
       width = 40, 
       height = 25,
       units = "cm")

trfsize_distros<-as.data.table(gr_trf)[,strand:=NULL][,annot:="TRF"]
satsize_distros<-as.data.table(gr_sat)[,strand:=NULL][,annot:="SAT"]

size_distros<-rbind(satsize_distros, trfsize_distros)
size_distros[,logwidth:=log10(width)]
setkey(size_distros, annot, seqnames, start)

size_distros %>%
    mutate(annot = factor(annot, levels=c("SAT", "TRF"))) %>%
  ggplot(aes(logwidth, annot, fill = annot)) + 
  geom_boxplot() + 
  ylab("Repeat annotation type") +
  xlab("log10 array lengths") +
  theme(axis.text.x=element_text(angle=90, vjust = 0.5, hjust=1)) +
  scale_fill_brewer(palette = "Dark2") -> TRARRAYSIZE #+
  # theme(strip.text.y = element_text(face = "bold"),
  #       axis.text.y = element_text(size=16),
  #       axis.text.x = element_text(size=16),
  #       legend.position = "none",
  #       plot.title = element_text(hjust = 0.5, size =22)) -> TRARRAYSIZE

#ggsave(TRARRAYSIZE, file ="/mnt/users/oymo/kristina_samarbeid/figurar/TRsize_box.png", 
#       width = 40, 
#       height = 25,
#       units = "cm")



teldist<-size_distros[,.(annot, width, distance)]
#sizecheck<-size_distros
#sizecheck<-unique(sizecheck[,chrlength:= max(end), by=seqnames][,.(seqnames,chrlength)])
distcheck<-size_distros
distcheck<-unique(distcheck[,maxdist:= max(distance), by=seqnames][,.(seqnames,maxdist)])


breaks<-seq(0, 63624047, length = 636)



#sizecheck[,bin:=findInterval(chrlength, breaks)]
#distcheck[,bin:=findInterval(maxdist, breaks)]


##OK storleiken på kromosomar gir meining, får sjå på avstandar til telomer
#teldist[,bin:=findInterval(distance, breaks)]
#etter bin 400 vert det ukonsekvent, nevn i fig.tekst

telcount<-teldist[,sum(width), by=.(bin, annot)]
telcount[annot=="TRF", V1:=-V1]
telcount$annot <- factor(telcount$annot, levels = c("SAT", "TRF"))
telcount[,nobin:=.N, by=bin]


telcount  %>%
  ggplot(aes(x=bin, y=V1, fill=annot)) +
  geom_histogram(stat="identity") +
  ylab("repDNA density") +
  xlab("Mb from nearest telomere") +
  theme(axis.text.x=element_text(angle=90, vjust = 0.5, hjust=1)) +
  scale_fill_brewer(palette = "Dark2") -> DISTOGRAM #+
  # theme(strip.text.y = element_text(face = "bold"),
  #       axis.text.y = element_text(size=16),
  #       axis.text.x = element_text(size=16),
  #       legend.title = element_blank(),
  #       plot.title = element_text(hjust = 0.5, size =22)) -> DISTOGRAM
  


ggsave(DISTOGRAM, file ="/mnt/users/oymo/kristina_samarbeid/figurar/TRtotelomere.png", 
       width = 40, 
       height = 25,
       units = "cm")


#PROPORTIONS_BARPLOT
#RIDGEPLOT
BOXPLOT
TRARRAYSIZE
DISTOGRAM
```

